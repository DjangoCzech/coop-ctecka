package com.pors.coopctecka.presentation.ui.recipelist

import androidx.compose.runtime.MutableState
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.pors.coopctecka.domain.model.Recipe
import com.pors.coopctecka.repository.RecipeRepository
import javax.inject.Named
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class RecipeListViewModel
@ViewModelInject
constructor(
    private val repository: RecipeRepository,
    private @Named("auth_token") val token: String,

): ViewModel(){
    val recipes: MutableState<List<Recipe>> = mutableStateOf(listOf())
    private val _recipes: MutableLiveData<List<Recipe>> = MutableLiveData()


    init {
        viewModelScope.launch {
            var result = repository.search(
                token = token,
                page = 1,
                query = "chicken",
            )
            _recipes.value = result

            /*suspend fun getRecipe(){
                val recipes = repository.get(token, 1)
                _recipes.value = recipes
            }*/
        }
        //println("VIEWMODEL: ${repository}")
        //println("VIEWMODEL: ${token}")
    }

    fun getRepo() = repository
    fun getToken() = token
}