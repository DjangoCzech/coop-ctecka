package com.pors.coopctecka.di

import com.pors.coopctecka.network.RecipeService
import com.pors.coopctecka.network.model.RecipeDtoMapper
import com.pors.coopctecka.repository.RecipeRepository
import com.pors.coopctecka.repository.RecipeRepository_Impl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun provideRecipeRepository(
        recipeService: RecipeService,
        recipeDtoMapper: RecipeDtoMapper
    ): RecipeRepository{
        return RecipeRepository_Impl(recipeService, recipeDtoMapper)
    }
}