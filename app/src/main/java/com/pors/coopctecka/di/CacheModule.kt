package com.pors.coopctecka.di

import androidx.room.Room
import com.pors.coopctecka.BaseApplication
import com.pors.coopctecka.data.db.ReaderDao
import com.pors.coopctecka.data.db.ReaderDatabase
import com.pors.coopctecka.data.model.RecipeEntityMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CacheModule {
    @Singleton
    @Provides
    fun provideDb(app: BaseApplication): ReaderDatabase{
        return Room
            .databaseBuilder(app, ReaderDatabase::class.java, ReaderDatabase.DATABASE_NAME)
            .build()
    }

    @Singleton
    @Provides
    fun provideRecipeDao(app: ReaderDatabase): ReaderDao{
        return app.readerDao()
    }

    @Singleton
    @Provides
    fun provideCacheRecipeMapper(): RecipeEntityMapper{
        return RecipeEntityMapper()
    }

}