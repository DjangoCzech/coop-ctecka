package com.pors.coopctecka.network.responses

import com.google.gson.annotations.SerializedName
import com.pors.coopctecka.network.model.RecipeDto

data class RecipeSearchResponse (
    @SerializedName("count")
    var count: Int,

    @SerializedName("results")
    var recipes: List<RecipeDto>
)