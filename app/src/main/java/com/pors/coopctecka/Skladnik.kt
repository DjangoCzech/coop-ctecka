package com.pors.coopctecka

data class Skladnici(
    val skladnici: ArrayList<Skladnik>
)

data class Skladnik(
    val prac: Int,
    val id: String,
    val heslo: String,
    val skladnik: String,
)
