package com.pors.coopctecka.interactors.recipe_list

import com.pors.coopctecka.data.db.ReaderDao
import com.pors.coopctecka.data.model.RecipeEntityMapper
import com.pors.coopctecka.domain.data.DataState
import com.pors.coopctecka.domain.model.Recipe
import com.pors.coopctecka.domain.util.RECIPE_PAGINATION_PAGE_SIZE
import com.pors.coopctecka.network.RecipeService
import com.pors.coopctecka.network.model.RecipeDtoMapper
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class SearchRecipes (
    private val recipeDao: ReaderDao,
    private val recipeService: RecipeService,
    private val entityMapper: RecipeEntityMapper,
    private val dtoMapper: RecipeDtoMapper,
){
    fun execute(
        token: String,
        page: Int,
        query: String,
    ): Flow<DataState<List<Recipe>>> = flow {
        try {
            emit(DataState.loading<List<Recipe>>())
            //just to show
            delay(1000)

            val recipes = getRecipesFromNetwork(token = token, page = page, query=query)

            //Insert to databaze
            recipeDao.insertRecipes(entityMapper.toEntityList(recipes))

            //query
            val cacheResult = if(query.isBlank()){
                recipeDao.getAllRecipes(
                    pageSize = RECIPE_PAGINATION_PAGE_SIZE,
                    page = page
                )
            } else {
                recipeDao.searchRecipes(
                    query = query,
                    pageSize = RECIPE_PAGINATION_PAGE_SIZE,
                    page = page
                )
            }
            //emit List<Recipe>
            val list = entityMapper.fromEntityList(cacheResult)
            emit(DataState.success(list))

        }catch (e:Exception){
            emit(DataState.error<List<Recipe>>(e.message?: "Neznama chyba"))
        }
    }
//chybu v pripade ze neni pripojeni
    private suspend fun getRecipesFromNetwork(
        token: String,
        page: Int,
        query: String,
    ): List<Recipe>{
        return dtoMapper.toDomainList(
            recipeService.search(
                token = token,
                page = page,
                query = query
            ).recipes
        )
    }
}