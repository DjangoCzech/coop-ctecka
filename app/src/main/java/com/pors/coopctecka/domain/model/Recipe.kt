package com.pors.coopctecka.domain.model

import java.util.*

//testovaci model pro Recipe
//pozdeji udelat ten samy pro ostatni tabulky z COOP


data class Recipe (
    val id: Int,
    val title: String,
    val publisher: String,
    val featuredImage: String,
    val rating: Int,
    val sourceUrl: String,
    val description: String,
    val cookingInstructions: String,
    val ingredients: List<String> = listOf(),
    val dateAdded: Date,
    val dateUpdated: Date,
        )