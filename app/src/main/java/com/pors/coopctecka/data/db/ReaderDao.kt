package com.pors.coopctecka.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pors.coopctecka.Skladnik
import com.pors.coopctecka.data.db.entity.RecipeEntity
import com.pors.coopctecka.data.db.entity.ZebraDoklad
import com.pors.coopctecka.data.db.entity.ZebraPolozka
import com.pors.coopctecka.data.db.entity.ZebraSkladnik
import com.pors.coopctecka.domain.util.RECIPE_PAGINATION_PAGE_SIZE

@Dao
interface ReaderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecipe(recipe: RecipeEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecipes(recipe: List<RecipeEntity>): LongArray

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSkladnik(zebraSkladnik: ZebraSkladnik)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDoklad(zebraDoklad: ZebraDoklad)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPolozka(zebraPolozka: ZebraPolozka)

    @Query("SELECT * FROM recipes WHERE id = :id")
    fun getRecipeById(id: Int): RecipeEntity?

    @Query("DELETE FROM cis06zebras")
    fun deleteAllSkladnik()

    @Query("SELECT * FROM cis06zebras")
    fun getAllSkladnik(): List<Skladnik>

    @Query("""
        SELECT * FROM recipes 
        ORDER BY date_updated DESC LIMIT :pageSize OFFSET ((:page - 1) * :pageSize)
    """)
    fun getAllRecipes(
        page: Int,
        pageSize: Int = RECIPE_PAGINATION_PAGE_SIZE
    ): List<RecipeEntity>

    @Query("""
        SELECT * FROM recipes 
        WHERE title LIKE '%' || :query || '%'
        OR ingredients LIKE '%' || :query || '%'  
        ORDER BY date_updated DESC LIMIT :pageSize OFFSET ((:page - 1) * :pageSize)
        """)
    fun searchRecipes(
        query: String,
        page: Int,
        pageSize: Int = RECIPE_PAGINATION_PAGE_SIZE
    ): List<RecipeEntity>

   /* @Query("select distinct sklad from cis06zebrap")
    fun getAllSklad(): List<Sklad>

    @Query("select distinct sklad,doklad from cis06zebrap order by sklad")
    fun getAllDoklad(): List<Polozka>*/


}