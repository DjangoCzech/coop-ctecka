package com.pors.coopctecka.data.network.response

import com.google.gson.annotations.SerializedName
import com.pors.coopctecka.data.db.entity.ZebraPolozka

data class PolozkyResponse(
    val nameArray: ZebraPolozka,
    @SerializedName("polozky")
    val polozkyResponse: PolozkyResponse
)
