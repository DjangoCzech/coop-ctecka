package com.pors.coopctecka.data.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.pors.coopctecka.data.db.entity.ZebraSkladnik
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


//http://10.0.1.23:5000/getSkladnik

interface SybaseSkladniciApiService {

    @GET("getSkladnik")
    fun getSkladnik(
        //@Query("lang") languageCode: String = "en"
    ): Deferred<List<ZebraSkladnik>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): SybaseSkladniciApiService {
            val requestInterceptor = Interceptor {chain ->
                val url = chain.request()
                    .url
                    .newBuilder()
                    .build()
                val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

                return@Interceptor chain.proceed(request)

            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addInterceptor(connectivityInterceptor) //zjistujeme, zda je pripojeni online
                .build()
            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("http://10.0.1.23:5000/")
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SybaseSkladniciApiService::class.java)

        }
    }
}