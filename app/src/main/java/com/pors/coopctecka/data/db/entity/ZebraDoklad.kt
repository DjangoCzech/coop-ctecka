package com.pors.coopctecka.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName



@Entity(tableName = "cis06zebraz")
data class ZebraDoklad (
        @SerializedName("doklad")
        @PrimaryKey val doklad: Int,
        @SerializedName("org")
        val org: Int,
        @SerializedName("odj")
        val odj: Int,
        @SerializedName("datuct")
        val datuct: String,
        @SerializedName("rozvoz")
        val rozvoz: Int,
        @SerializedName("stav")
        val stav: Int,
        @SerializedName("pocpol")
        val pocpol: Int,
        @SerializedName("prac")
        val prac: Int
        )