package com.pors.coopctecka.data.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.pors.coopctecka.internal.NoConnectivityException
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ConnectivityInterceptorImpl(
        context: Context
) : ConnectivityInterceptor {

    private val appContext = context.applicationContext

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isOnline(appContext)){
                throw NoConnectivityException("Problem s pripojenim na server")
            } else {
            return chain.proceed(chain.request())
        }


    }

    fun isOnline(context: Context) =
        (appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).run{
            getNetworkCapabilities(activeNetwork)?.run {
                hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                        || hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                        || hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
            } ?: false
        }

//    private fun isOnlineLong():Boolean{
//        val connectivityManager = appContext.getSystemService(Context.CONNECTIVITY_SERVICE)
//        as ConnectivityManager
//        val networkInfo = connectivityManager.activeNetwork
//        return networkInfo != null && networkInfo
//
//    }
}