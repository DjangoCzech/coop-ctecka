package com.pors.coopctecka.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "cis06zebrap")
data class ZebraPolozka(
    @SerializedName("doklad")
     val doklad: Int,
    @SerializedName("sklad")
    val sklad: Int,
    @SerializedName("reg")
    val reg: Int,
    @SerializedName("mnoz_obj")
    val mnoz_obj: String,
    @SerializedName("mnoz_vyd")
    val mnoz_vyd: String,
    @SerializedName("kc_pce")
    val kc_pce: String,
    @SerializedName("sarze")
    val sarze: Int,
    @SerializedName("datspo")
    val datspo: String,
    @SerializedName("veb")
    val veb: String,
    @SerializedName("poc2")
    val poc2: Int,
    @SerializedName("pvp06pk")
    @PrimaryKey val pvp06pk: String,
    @SerializedName("znacky")
    val znacky: String,
    @SerializedName("stav")
    val stav: Int,
    @SerializedName("prac")
    val prac: Int
)

