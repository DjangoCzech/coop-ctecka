package com.pors.coopctecka.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "cis06zebras")
data class ZebraSkladnik(
    @SerializedName("prac")
    @PrimaryKey val prac: Int,
    @SerializedName("id")
    val id: String,
    @SerializedName("heslo")
    val heslo: String,
    @SerializedName("skladnik")
    val skladnik: String
)


