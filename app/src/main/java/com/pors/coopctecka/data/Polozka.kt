package com.pors.coopctecka.data


import com.google.gson.annotations.SerializedName

data class dPolozka(
    val datspo: String,
    val doklad: String,
    @SerializedName("kc_pce")
    val kcPce: String,
    @SerializedName("mnoz_obj")
    val mnozObj: String,
    @SerializedName("mnoz_vyd")
    val mnozVyd: String,
    val poc2: String,
    val prac: String,
    val pvp06pk: String,
    val reg: String,
    val sarze: String,
    val sklad: String,
    val stav: String,
    val veb: String,
    val znacky: String
)