package com.pors.coopctecka.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.pors.coopctecka.data.db.entity.RecipeEntity
import com.pors.coopctecka.data.db.entity.ZebraDoklad
import com.pors.coopctecka.data.db.entity.ZebraPolozka
import com.pors.coopctecka.data.db.entity.ZebraSkladnik

@Database(
    entities = [ZebraDoklad::class, ZebraPolozka::class, ZebraSkladnik::class, RecipeEntity::class],
    version = 1, exportSchema = false
)
abstract class ReaderDatabase:RoomDatabase() {
    abstract fun readerDao(): ReaderDao

    companion object {
        val DATABASE_NAME: String = "horovice06"
        /*@Volatile
        private var instance: ReaderDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                ReaderDatabase::class.java, "horovice06")
                .build()*/

//        fun buildDatabase(context: Context, scope: CoroutineScope):ReaderDatabase{
//            return INSTANCE?: synchronized(this){
//                val instance = Room.databaseBuilder(context.applicationContext, ReaderDatabase::class.java, "horovice06")
//                    .fallbackToDestructiveMigration()
//                    .addCallback(ReaderDatabaseCallback(context, scope))
//                    .build()
//                INSTANCE = instance
//                instance
//            }
//        }


//    private class ReaderDatabaseCallback(
//        private val context: Context,
//        private val scope: CoroutineScope
//    ) : RoomDatabase.Callback() {
//        override fun onOpen(db: SupportSQLiteDatabase) {
//            super.onOpen(db)
//            INSTANCE?.let { database ->
//                scope.launch(Dispatchers.IO) {
//                    val jsonObj = JsonParser.parseString(readJsonFromFile(context)).asJsonObject
//
//                    val zebraSkladnikType = object : TypeToken<ZebraSkladnik>() {}.type
//                    val zebraSkladnik: ZebraSkladnik = Gson().fromJson(jsonObj, zebraSkladnikType)
//
//                    populateDatabaseSkladnik(database, zebraSkladnik)
//
//                }
//            }
//        }
//    }

//        private fun populateDatabaseSkladnik(database: ReaderDatabase, zebraSkladnik: ZebraSkladnik) {
//            val readerDao = database.readerDao()
//            readerDao.deleteAllSkladnik()
//            Log.d("ReaderDatabase","Naplnujeme data")
//            val skladnici = zebraSkladnik
//
//
//            zebraSkladnik.let {
//                for (item in zebraSkladnik){
//                    readerDao.insertSkladnik(item)
//                    Log.d("ReaderDatabase","${item.toString()}")
//                }
//            }
//        }

//        fun readJsonFromFile(context: Context): String? {
//            val apiService = SybaseSkladniciApiService(ConnectivityInterceptorImpl(context))
//            val getPolozkyResponse: String = ""
//            scope.launch(Dispatchers.IO) {
//                try {
//                    val getPolozkyResponse = apiService.getSkladnik().await()
//                } catch (e: NoConnectivityException) {
//                    Log.d("No connectivity", "Neni pripojeni")
//                }
//            }
//            return getPolozkyResponse
//
//
//        }

    }
}
