package com.pors.coopctecka.internal

import android.widget.Toast
import java.io.IOException

class NoConnectivityException(message: String): IOException(message)