package com.pors.coopctecka



class Polozka {
    var doklad: String = ""
    var sklad: Int = 0
    var reg: String = ""
    var mnoz_obj: Int = 0
    var mnoz_vyd: Int = 0
    var kc_pce: Float = 0.0f
    var sarze: String = ""
    var datspo: String = ""
    var veb: Float = 0.0f
    var poc2: Int = 0
    var pvp06pk: String = ""
    var znacky: String = ""
    var stav: Int = 0
    var prac_pol: String = ""
}