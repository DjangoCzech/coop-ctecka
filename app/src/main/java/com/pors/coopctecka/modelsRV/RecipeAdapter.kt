package com.pors.coopctecka.modelsRV

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pors.coopctecka.R
import com.pors.coopctecka.domain.model.Recipe
import com.pors.coopctecka.network.model.RecipeDtoMapper
import kotlinx.android.synthetic.main.item_layout.view.*

class RecipeAdapter(val context: Context, val items: List<Recipe>):
        RecyclerView.Adapter<RecipeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.get(position)
        holder.id_tv.text = item.id.toString()
        holder.name_tv.text = item.description.toString()
        holder.payment_tv.text = item.title.toString()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val id_tv = view.id_tv
        val name_tv = view.name_tv
        val payment_tv = view.payment_tv

    }
}



