package com.pors.coopctecka.modelsRV

data class Skladnici(
    val skladnici: ArrayList<SkladnikModelClass>
)

data class SkladnikModelClass(
val prac: Int,
val id: String,
val heslo: String,
val skladnik: String,
)