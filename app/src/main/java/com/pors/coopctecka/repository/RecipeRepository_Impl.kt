package com.pors.coopctecka.repository

import com.pors.coopctecka.domain.model.Recipe
import com.pors.coopctecka.domain.util.DomainMapper
import com.pors.coopctecka.network.RecipeService
import com.pors.coopctecka.network.model.RecipeDto
import com.pors.coopctecka.network.model.RecipeDtoMapper

class RecipeRepository_Impl(
    private val recipeService: RecipeService,
    private val mapper: RecipeDtoMapper
): RecipeRepository {
    override suspend fun search(token: String, page: Int, query: String): List<Recipe> {
        return mapper.toDomainList(recipeService.search(token, page, query).recipes)
    }

    override suspend fun get(token: String, id: Int): Recipe {
        return mapper.mapToDomainModel(recipeService.get(token, id))
    }
}