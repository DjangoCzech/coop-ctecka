package com.pors.coopctecka

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class PaymentAdapter(val context:Context, var payment_list: ArrayList<PaymentModel>,
private val listener: OnItemClickListener):
    RecyclerView.Adapter<PaymentAdapter.ViewHolder>() {

    var selected_position: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (payment_list != null && payment_list.size > 0){
            holder.itemView.setBackgroundColor(if (selected_position==position) Color.GREEN else Color.TRANSPARENT)
            val modelItem = payment_list[position]
            holder.id_tv.text = modelItem.id
            holder.name_tv.text = modelItem.name
            holder.payment_tv.text = modelItem.payment


        } else{
            return
        }

    }

    override fun getItemCount(): Int {
        return if (payment_list.isNotEmpty()) payment_list.size else 0
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view),
        View.OnClickListener{

        val id_tv: TextView = view.findViewById(R.id.id_tv)
        val name_tv: TextView = view.findViewById(R.id.name_tv)
        val payment_tv: TextView = view.findViewById(R.id.payment_tv)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            val position: Int = adapterPosition
            if (position != RecyclerView.NO_POSITION){
                listener.onItemClick(position)
                notifyItemChanged(selected_position)
                selected_position = adapterPosition
                notifyItemChanged(selected_position)
            }

        }


    }
    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }

}