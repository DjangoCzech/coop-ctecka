package com.pors.coopctecka

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.*
import com.google.gson.stream.JsonReader
import com.pors.coopctecka.data.db.ReaderDao
import com.pors.coopctecka.data.db.entity.RecipeEntity
import com.pors.coopctecka.data.db.entity.ZebraSkladnik
import com.pors.coopctecka.data.model.RecipeEntityMapper
import com.pors.coopctecka.data.network.ConnectivityInterceptorImpl
import com.pors.coopctecka.data.network.SybaseSkladniciApiService
import com.pors.coopctecka.domain.model.Recipe
import com.pors.coopctecka.domain.util.RECIPE_PAGINATION_PAGE_SIZE
import com.pors.coopctecka.interactors.recipe_list.SearchRecipes
import com.pors.coopctecka.internal.NoConnectivityException
import com.pors.coopctecka.modelsRV.RecipeAdapter
import com.pors.coopctecka.modelsRV.SkladnikAdapter
import com.pors.coopctecka.modelsRV.Skladnici
import com.pors.coopctecka.network.RecipeService
import com.pors.coopctecka.network.model.RecipeDtoMapper
import com.pors.coopctecka.presentation.ui.recipelist.RecipeListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import org.json.JSONObject
import retrofit2.Retrofit
import java.io.IOException
import java.io.StringReader
import java.lang.reflect.Type
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.nio.charset.Charset
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import javax.inject.Inject
@AndroidEntryPoint
class MainActivity : AppCompatActivity(), PaymentAdapter.OnItemClickListener {

    private val TAG = "MainActivity"
    val adapter = PaymentAdapter(this, ArrayList(), this)
    private var linearLayoutManager: LinearLayoutManager? = null
    private val exampleList = getList()
    private val client = OkHttpClient()
    private val viewModel: RecipeListViewModel by viewModels()
    private val recipeDao: ReaderDao? = null
    private val entityMapper: RecipeEntityMapper? = null
    private val scope = CoroutineScope(newSingleThreadContext("name"))




    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doklad_sklady)
        val recycler_view_sklady = findViewById<RecyclerView>(R.id.recycler_view_sklady)
        val recyclerview_doklady = findViewById<RecyclerView>(R.id.recyclerview_doklady)
        val textView2 = findViewById<TextView>(R.id.textView2)
        val btn_button2 = findViewById<Button>(R.id.button2)

//        linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
//        recycler_view_sklady?.layoutManager = linearLayoutManager
//        recycler_view_sklady?.adapter = getList()?.let { PaymentAdapter(this, it, this) }
//        recycler_view_sklady?.setHasFixedSize(true)

        btn_button2.setOnClickListener {
            runJson()
        }


        Log.d(TAG, "start json gson")
        println("FRAGMENT: ${viewModel.getRepo()}")
        println("FRAGMENT: ${viewModel.getToken()}")




       /* val apiService = SybaseSkladniciApiService(ConnectivityInterceptorImpl(this.applicationContext!!))
        try {
            val getPolozkyResponse = apiService.getSkladnik()
            textView2.text = getPolozkyResponse.toString()
        } catch (e: NoConnectivityException){
            textView2.text = "Problem s pripojenim"
        }*/

        //val apiService =
//            SybaseSkladniciApiService(ConnectivityInterceptorImpl(this.applicationContext!!))
//        GlobalScope.launch(Dispatchers.Main) {
//
//            try {
//                val getPolozkyResponse = apiService.getSkladnik().await()
//                textView2.text = getPolozkyResponse.toString()
//            } catch (e: NoConnectivityException) {
//                textView2.text = "Problem s pripojenim"
//            }
//        }


        //val jsonFileString = getJsonDataFromAsset(applicationContext, "skladnici.json")
        //if (jsonFileString != null) {
        //    Log.i(TAG, jsonFileString)
        //}
        //val gson = Gson()
        //val listPersonType = object : TypeToken<List<Skladnik>>() {}.type
        //var skladnici: Skladnik = gson.fromJson(jsonFileString, listPersonType)
        //var skladnici: List<skladnik> = gson.fromJson(jsonFileString, listPersonType)
        //skladnici.forEachIndexed{idx, person -> Log.i("data","> Item $idx:\n$person")}
//
        //var jsonFileString = getJsonDataFromAsset(applicationContext, "skladnici.json")
        //var listoPrint: List<Skladnik>? = jsonFileString?.let { getObjectFromJsonArray(it) }
        insertRecipeDatabase()

        val recipes: List<RecipeEntity>? =
                    recipeDao?.getAllRecipes(1, RECIPE_PAGINATION_PAGE_SIZE)
        val list = recipes?.let { entityMapper?.fromEntityList(it) }

        if (list != null) {
            for (i in list){
                Log.d("getList", "${i.description}")
            }
        }




        try {
            //val jsonString = getJSONFromAssets()
            //val skladnici = Gson().fromJson(jsonString, Skladnici::class.java)
            recycler_view_sklady.layoutManager = LinearLayoutManager(this)
            recyclerview_doklady.layoutManager = LinearLayoutManager(this)
            val recipeAdapter = list?.let { RecipeAdapter(this, it) }
            recycler_view_sklady.adapter = recipeAdapter
            //val itemAdapter = SkladnikAdapter(this, skladnici.skladnici)
            //recycler_view_sklady.adapter = itemAdapter

        } catch (e:JSONException){
            e.printStackTrace()
        }
    }

    fun insertRecipeDatabase(){
        scope.launch{
            val recipeDao: ReaderDao? = null
            val recipeService: RecipeService? = null
            val entityMapper: RecipeEntityMapper? = null
            val dtoMapper: RecipeDtoMapper? = null
            val recipes = recipeService?.search(
                    token = "Token 9c8b06d329136da358c2d00e76946b0111ce2c48",
                    page = 1,
                    query = "Chicken"
                )?.let {
                    dtoMapper?.toDomainList(
                        it.recipes
                    )
                }
                if (recipeDao != null) {
                    if (entityMapper != null) {
                        recipes?.let { entityMapper.toEntityList(it) }?.let { recipeDao.insertRecipes(it) }
                    }
                }
        }
    }


    fun getList(): ArrayList<PaymentModel>? {
        Log.d(TAG, "volame metodu getList - start")

        val paymentList: ArrayList<PaymentModel> = arrayListOf()
        paymentList.add(PaymentModel("1", "Jan", "200000"))
        paymentList.add(PaymentModel("2", "Josef", "10000"))
        paymentList.add(PaymentModel("3", "Karel", "140000"))
        paymentList.add(PaymentModel("4", "Jana", "130000"))
        paymentList.add(PaymentModel("5", "Lucie", "160000"))
        paymentList.add(PaymentModel("6", "Dorka", "160000"))
        Log.d(TAG, "vracime hodnoty seznamu ${paymentList.toString()}")
        return paymentList
    }
    fun runJson() {
        val urlBase = "http://10.86.10.210:12345/test/"
        val request = Retrofit.Builder()
            .baseUrl(urlBase)
            .build()
        val service = request.create(APIService::class.java)
        val answer = JSONObject()
        answer.put("name", "Honza")
        answer.put("id", "5")

        val answerString = answer.toString()
        val requestBody = answerString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            val response = service.createTest(requestBody)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    val gson = GsonBuilder().setPrettyPrinting().create()
                    val prettyJson = gson.toJson(
                        JsonParser.parseString(
                            response.body()
                                ?.string()
                        )
                    )
                    Log.d(TAG, prettyJson)
                } else {
                    Log.e(TAG, response.code().toString())
                }
            }
        }


        //Toast.makeText(this, answer.toString(), Toast.LENGTH_SHORT).show()

    }


    override fun onItemClick(position: Int) {
        Toast.makeText(this, "Item $position clicked", Toast.LENGTH_SHORT).show()
        val clickedItem: PaymentModel = exampleList!![position]
        adapter.notifyItemChanged(position)
    }
//    fun getObjectFromJsonArray(jsonArrayStr: String): List<Skladnik> {
//        //var stringReader: StringReader = StringReader(getJsonDataFromAsset(applicationContext, "skladnici.json"))
//        var stringReader: StringReader = StringReader(jsonArrayStr)
//        //var jsonReader: JsonReader = JsonReader(stringReader)
//        val gsonBuilder = GsonBuilder().serializeNulls()
//        gsonBuilder.registerTypeAdapter(Skladnik::class.java, SkladnikDeserializer())
//        val gson = gsonBuilder.create()
//        val skladnikList: List<Skladnik> = gson.fromJson(stringReader, Array<Skladnik>::class.java).toList()
//
//        return skladnikList
//    }
//    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
//        val jsonString: String
//        try {
//            jsonString = context.assets.open(fileName).bufferedReader().use{ it.readText()}
//        } catch (ioException: IOException){
//            ioException.printStackTrace()
//            return null
//        }
//        return jsonString
//    }


//    class SkladnikDeserializer : JsonDeserializer<Skladnik> {
//        override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?
//        ): Skladnik {
//            val jsonObj = json as JsonObject
//            val skladnici = Skladnik()
//
//            val jsonSkladnikObject = jsonObj.getAsJsonObject("skladnici")
//            //val jsonPolozkaObject = jsonObj.getAsJsonObject("polozky")
//            skladnici.id = jsonSkladnikObject.get("id").asString
//            skladnici.heslo = jsonSkladnikObject.get("heslo").asString
//            skladnici.prac = jsonSkladnikObject.get("prac").asInt
//            skladnici.skladnik = jsonSkladnikObject.get("skladnik").asString
//
//            return skladnici
//        }
//
//    }

    private fun getJSONFromAssets():String? {
        var json: String?
        val charset: Charset = Charsets.UTF_8
        try {
            val myUserJsonFile = assets.open("skladnici.json")
            val size = myUserJsonFile.available()
            val buffer = ByteArray(size)
            myUserJsonFile.read(buffer)
            myUserJsonFile.close()
            json = String(buffer, charset)
        } catch (ex: IOException){
            ex.printStackTrace()
            return null
        }
        return json
    }

}